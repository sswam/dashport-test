# Dashport Test

This is a minimal test app for [Dashport](https://github.com/oslabs-beta/dashport) using Google auth,
based on their README.

Dashport is an authentication framework for [Deno](https://deno.land/).

Copy `dashportconfig.ts.dist` to `dashportconfig.ts`, and enter your Google auth client ID and client secret.
